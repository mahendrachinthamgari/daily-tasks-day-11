const fs = require('fs');
const path = require('path');

const filePath1 = path.join(__dirname, './file1.txt');
const filePath2 = path.join(__dirname, './file2.txt');
const lipsumFilePath = path.join(__dirname, './lipsum.txt');
const newLipsumFilePath = path.join(__dirname, './new_lipsum.txt');
const savedActivityFilePath = path.join(__dirname, './saved_activity.txt');



/*
NOTE: Do not change the name of this file

NOTE: For all file operations use promises with fs. Promisify the fs callbacks rather than use fs/promises.

Q1. Create 2 files simultaneously (without chaining).
Wait for 2 seconds and starts deleting them one after another. (in order)
(Finish deleting all the files no matter what happens with the creation code. Ensure that this is tested well)

*/

createFiles(filePath1, filePath2)
    .then((message) => {
        console.log(message);
    })
    .catch((err) => {
        console.error(err);
    })
    .then(() => {
        return deleteFile(filePath1);
    })
    .then((message) => {
        console.log(message);
        return deleteFile(filePath2);
    })
    .then((message) => {
        console.log(message);
    })
    .catch((err) => {
        console.error(err);
    })


function createFiles(filePath1, filePath2) {
    return new Promise((resolve, reject) => {
        fs.writeFile(filePath1, JSON.stringify("Created new file1"), (err) => {
            if (err) {
                console.error(err);
            } else {
                console.log('file1 created sucessfully');

                fs.writeFile(filePath2, JSON.stringify("Created new file2"), (err) => {
                    if (err) {
                        reject(err);
                    } else {
                        console.log('File2 created sucessfully');
                        resolve("Files Created Sucessfully");
                    }
                })
            }
        })
    })
}


function deleteFile(filePath) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            fs.unlink(filePath, (err) => {
                if (err) {
                    reject(err);
                } else {
                    resolve("File deleted sucessfully");
                }
            })

        }, 2 * 1000);
    })

}



/*
Q2. Create a new file with lipsum data (you can google and get this). 
Do File Read and write data to another file
Delete the original file 
Using promise chaining
*/


function readLipsumData(lipsumFilePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(lipsumFilePath, "utf-8", (err, data) => {
            if (err) {
                reject(err);
            } else {
                console.log('file read sucessful');
                resolve(data.toString());
            }
        })
    })
}

function writeLipsumData(newLipsumFilePath, data) {
    return new Promise((resolve, reject) => {
        fs.writeFile(newLipsumFilePath, data, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve('File write sucessfull');
            }
        })
    })
}

function deleteOriginalFile(lipsumFilePath) {
    return new Promise((resolve, reject) => {
        fs.unlink(lipsumFilePath, (err) => {
            if (err) {
                reject(err);
            } else {
                resolve('Original File deleted sucessfully');
            }
        })
    })
}


readLipsumData(lipsumFilePath)
    .then((data) => {
        return writeLipsumData(newLipsumFilePath, data);
    })
    .then((message) => {
        console.log(message);
        return deleteOriginalFile(lipsumFilePath)
    })
    .then((message) => {
        console.log(message);
    })
    .catch((err) => {
        console.error(err);
    })




/*
Q3.
Use appropriate methods to
A. login with value 3 and call getData once login is successful
B. Write logic to logData after each activity for each user. Following are the list of activities
    "Login Success"
    "Login Failure"
    "GetData Success"
    "GetData Failure"

    Call log data function after each activity to log that activity in the file.
    All logged activity must also include the timestamp for that activity.

    You may use any data you want as the `user` object as long as it represents a valid user (eg: For you, for your friend, etc)
    All calls must be chained unless mentioned otherwise.

*/


function login(user, val) {
    if (val % 2 === 0) {
        return Promise.resolve(user);
    } else {
        return Promise.reject(new Error("User not found"));
    }
}

function getData() {
    return Promise.resolve([
        {
            id: 1,
            name: "Test",
        },
        {
            id: 2,
            name: "Test 2",
        }
    ]);
}

function logData(user, activity) {
    // use promises and fs to save activity in some file
    return new Promise((resolve, reject) => {
        if (activity) {
            fs.writeFile(savedActivityFilePath, activity, (err) => {
                if (err) {
                    console.error(err);
                } else {
                    resolve(user)
                }
            })
        } else {
            reject(user);
        }
    })
}



login('User1', 3)
    .then(() => {
        return getData()
    })
    .then((data) => {
        console.log(data);
    })
    .catch((err) => {
        console.error(err);
    })



login('user1', 2)
.then(() => {
    console.log();
})